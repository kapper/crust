FROM crystallang/crystal:0.28.0 as compiler
RUN mkdir /src
COPY . /src
WORKDIR /src
RUN mkdir bin
RUN shards install && crystal build src/crust.cr --static -o bin/crust

FROM scratch
COPY --from=compiler /src/bin/crust .
EXPOSE 3000
CMD ["./crust"]

